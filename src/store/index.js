import Vue from 'vue'
import Vuex from 'vuex'
// import VueParticles from 'vue-particles'
import example from './module-example'
Vue.use(Vuex)
// Vue.use(VueParticles)

// const store = new Vuex.Store({
//   modules: {
//     example
//   }
// })
//
// export default store
export default function ({ store }) {
  // IMPORTANT! Instantiate Store inside this function

  const Store = new Vuex.Store({
    modules: {
      example
    }
  })

  return Store
}

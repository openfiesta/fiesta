import { Quasar } from 'quasar'
export default {
  locale: Quasar.i18n.getLocale()
}

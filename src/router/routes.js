
export default [
  {
    path: '/',
    component: () => import('layouts/default'),
    children: [
      { path: '', component: () => import('pages/index') },
      { path: '/lab', component: () => import('pages/lab') },
      { path: '/project', component: () => import('pages/project') },
      { path: '/project/:projectID', component: () => import('pages/projectdetail') },
      { path: '/notice', component: () => import('pages/update/notice') },
      { path: '/notice/:noticeID', component: () => import('pages/update/noticedetail') },
      { path: '/events', component: () => import('pages/update/events') },
      { path: '/events/:eventID', component: () => import('pages/update/eventsdetail') },
      { path: '/overview', component: () => import('pages/about/overview') },
      { path: '/cooperation', component: () => import('pages/cooperation') },
      { path: '/contact', component: () => import('pages/about/contact') },
      { path: '/campusmap', component: () => import('pages/campus/campusmap') },
      { path: '/calendar', component: () => import('pages/update/calendar') },
      { path: '/bio', component: () => import('pages/program/bio') },
      { path: '/bio/:id', component: () => import('pages/program/biomentor') },
      { path: '/iid', component: () => import('pages/program/iid') },
      { path: '/iid/:id', component: () => import('pages/program/iidmentor') },
      { path: '/video', component: () => import('pages/about/video') },
      { path: '/news', component: () => import('pages/update/news') },
      { path: '/news/:newsID', component: () => import('pages/update/newsdetail') },
      { path: '/recruit', component: () => import('pages/about/recruit') },
      { path: '/committee', component: () => import('pages/about/committee') },
      { path: '/student/oversea', component: () => import('pages/student/oversea') },
      { path: '/student/hmt', component: () => import('pages/student/hmt') },
      { path: '/student/china', component: () => import('pages/student/china') },
      { path: '/bioprogram', component: () => import('pages/program/bioprogram') },
      { path: '/iidprogram', component: () => import('pages/program/iidprogram') },
      { path: '/photowall', component: () => import('pages/campus/photowall') },
      { path: '/photowall/:photowallID', component: () => import('pages/campus/photodetail') },
      { path: '/achievement', component: () => import('pages/achievement') }
    ]
  },

  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
